# import the main window object (mw) from aqt
from aqt import mw
# import the "show info" tool from utils.py
from aqt.utils import showInfo, qconnect
# import all of the Qt GUI library
from aqt.qt import *

from aqt import gui_hooks

# We're going to add a menu item below. First we want to create a function to
# be called when the menu item is activated.

def testFunction() -> None:
    # get the number of cards in the current collection, which is stored in
    # the main window
    cardCount = mw.col.cardCount()
    # show a message box
    showInfo("Card count: %d" % cardCount)

def addTextboxToCard(text, card, kind) -> str:
    # TODO prevent erasing the text when showing answer
    content = ""
    if kind == "reviewAnswer":
        content = card.__dict__["_textbox_content"]
    text_area = f'<br><textarea class="textbox" rows="5" cols="60">{content}</textarea>'
    text += text_area
    return text

def saveTextboxContent(card, tags):
    # TODO get the text area content -> look at whiteboard plugin for how to use webview
    card.__dict__["_textbox_content"] = "content"

gui_hooks.card_will_show.append(addTextboxToCard)
gui_hooks.reviewer_will_play_answer_sounds.append(saveTextboxContent)

# create a new menu item, "test"
action = QAction("test", mw)
# set it to call testFunction when it's clicked
qconnect(action.triggered, testFunction)
# and add it to the tools menu
mw.form.menuTools.addAction(action)
